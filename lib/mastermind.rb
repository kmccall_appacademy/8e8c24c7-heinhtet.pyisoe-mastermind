class Code
  attr_reader :pegs
  attr_accessor :secret_code

  PEGS = {red: "r", green: "g", blue: "b",
          yellow: "y", orange: "o", purple: "p"}

  def initialize(array_of_pegs)
    @pegs = array_of_pegs
    @secret_code = @pegs
  end


  INVALIDS = "acdefhijklmnqstuvwxz1234567890-=[]\;',./"
  #convert strings to secret_code object
  def self.parse(thing)
    arr = thing.downcase.split('')
    arr.each {|el| raise "INVALID COLOR DETECTED" if INVALIDS.include?(el)}
    Code.new([arr[0],arr[1],arr[2],arr[3]])
  end

  def self.random
    Code.new([PEGS.values[rand(0..5)], PEGS.values[rand(0..5)],
    PEGS.values[rand(0..5)], PEGS.values[rand(0..5)]])
  end

  def [](num)
    @pegs[num]
  end

  # def ==(string)
  #   arr = string.split('')
  #   return true if @pegs == arr
  #   false
  # end

  def ==(thing)
    if thing.class == Code
      puts "ourpegs: #{@pegs}  otherpegs #{thing.secret_code} "
      return true if thing.secret_code == @pegs
    else
      false
    end
    false
  end

  def exact_matches(codeobject)
    return 4 if codeobject.secret_code == @pegs
    exacts = 0
    counter = 0
    while counter < 4
      # puts "guess: #{codeobject.secret_code[counter]}  secret: #{@pegs[counter]} "
      exacts += 1 if codeobject.secret_code[counter] == @pegs[counter]
      counter += 1
    end
    exacts
  end

  def near_matches(codeobject)
    nears = 0
    codeobject.secret_code.uniq.each_with_index do |el, idx|
      # puts "guess: #{el}  secret: #{@pegs}"
      nears += 1 if @pegs.include?(el)
    end

    nears - exact_matches(codeobject)
  end

end

class Game
  attr_accessor :secret_code
  attr_accessor :parse

  def initialize(new_code=Code.random)

    puts "Welcome. I have four balls of mystery."
    puts "The balls can come in 6 colors: Red, Green, Blue, Yellow, Orange, and Purple"
    @secret_code = new_code
    @won = false
  end

  def get_guess
    puts "Guess the colors now."
    puts "For example, type 'rrrr' for 4 reds or 'yopp' for yellow, orange, purple, purple."
    guess = Code.new(Code.parse($stdin.gets))
    puts "Your guess is: #{guess.secret_code}"
    guess
  end

  def play

    10.times do
      display_matches(get_guess)
      if get_guess == @secret_code
          break
          @won = true
      end
    end
    puts "CONGRATULATIONS" if @won == true 

  end

  def display_matches(yourguess)
    # puts "Exact means you matched color and position. Near means you matched only one"
    puts "Exact Matches: #{@secret_code.exact_matches(yourguess)}"
    puts "Near Matches: #{@secret_code.near_matches(yourguess)}"
  end



end
